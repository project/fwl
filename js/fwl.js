Drupal.behaviors.field_widget_layout = {
  attach: function (context, settings) {
    let tagName = context.tagName;
    let allowedTagNames = ["FORM", "BODY"];

    if (allowedTagNames.includes(tagName) || context instanceof Document) {
      let hasLayoutElements =
        context.getElementsByClassName("field-widget-layout").length > 0;
      if (hasLayoutElements) {
        let wrappers = context.getElementsByClassName("field-widget-layout");
        [...wrappers].forEach(function (wrapper) {
          wrapper.parentElement.classList.add("field-widget-layout--wrapper");
          let children = wrapper.parentElement.children;

          [...children].forEach(function (child) {
            if (child.hasAttribute("data-field-widget-layout-width")) {
              child.style.width = child.dataset.fieldWidgetLayoutWidth + "%";
            } else {
              child.style.width = "100%";
              child.classList.add("field-widget-layout");
            }

            if (child.hasAttribute("data-field-widget-layout-max-width")) {
              child.style.maxWidth =
                child.dataset.fieldWidgetLayoutMaxWidth + "px";
              child.style.minWidth = 100 + "px";
            } else {
              child.style.maxWidth = "100%";
              child.classList.add("field-widget-layout");
            }
          });
        });
      }
    }
  },
};

# Field Widget Layout (FWL)

With the module, users can adjust the width of fields in the edit form to better suit their needs. Users can set the width individually for each field.

Edit the width of a field in "Manage form display"

<?php

namespace Drupal\fwl\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * My Module settings form.
 */
class FwlSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['fwl.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fwl_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fwl.settings');

    $form['admin_route'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('FWL'),
      '#default_value' => $config->get('admin_route'),
      '#description' => $this->t('Show on all routes.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('fwl.settings');
    $config->set('admin_route', $form_state->getValue('admin_route'));
    $config->save();

    parent::submitForm($form, $form_state);
    $this->messenger()->addMessage($this->t('Configuration saved.'));
  }

}
